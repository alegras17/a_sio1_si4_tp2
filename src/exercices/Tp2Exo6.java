package exercices;

import java.util.Scanner;


public class Tp2Exo6 {


    public static void main(String[] args) {

        float taille,age;
        String sexe;
        int morph;
        float lorentz,creff;
        
        Scanner clavier = new Scanner(System.in);
        
        System.out.println("Quelle est votre taille en cm ?");
        taille = clavier.nextFloat();
        System.out.println("");
        
        System.out.println("Quel est votre age ?");
        age = clavier.nextFloat();
        System.out.println("");
        
        System.out.println("Quel est votre sexe ? (F ou M)");
        sexe = clavier.next();
        System.out.println("");
        
        System.out.println("Quelle est votre morphologie ? (1, 2 ou 3)\n 1: Fine, 2: Normale, 3:Large");
        morph = clavier.nextInt();
        System.out.println("");
        
        if( "F".equals(sexe) ){
            
            lorentz = (float) (taille-100-(taille-150)/2.5);
                        
        }
        
        else{
            
            lorentz = taille-100-(taille-150)/4;
            
        }
        
        if ( morph == 1){
            
            creff = (float) ((taille-100+age/10)*0.9*0.9);
            
        }
        
        else if ( morph == 2) {
            
            creff = (float) ((taille-100+age/10)*0.9);
            
        }
        
        else{
            
            creff = (float) ((taille-100+age/10)*0.9*1.1);
            
        } 
            
            
        System.out.println("Poids ideal selon la formule de Lorentz :" + lorentz );
        System.out.println("Poids ideal selon la formule de Creff :" + creff );
        
    }
}
