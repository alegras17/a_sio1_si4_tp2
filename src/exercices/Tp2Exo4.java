package exercices;

import java.util.Scanner;


public class Tp2Exo4 {


    public static void main(String[] args) {

        float ca;
        float com;
        Scanner clavier = new Scanner(System.in);
        
        System.out.println("Entrez votre chiffre d'affaire : ");
        ca = clavier.nextFloat();
        System.out.println();
        
        if ( ca < 10000 ) {
            
            com = (float) (ca * 0.02);
            System.out.println("Votre commission est de 2%\n");
        }
        
        else if ( 10000 <= ca && ca < 20000 ){
            
            com = (float) (200+(ca-10000)*0.04);
            System.out.println("Votre commission est de 4%\n");
        }
        
        else {
            
            com = (float) (600+(ca-20000)*0.06);
            System.out.println("Votre commission est de 6%\n");
        }
        
        System.out.println("Votre commission est de :" + com + "\n");
        
        
    }
}
