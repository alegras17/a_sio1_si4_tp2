package exercices;

import java.util.Scanner;


public class Tp2Exo5 {


    public static void main(String[] args) {

        float taille,poids;
        float imc;
        Scanner clavier = new Scanner(System.in);
        
        System.out.println("Entrez votre taille :");
        taille = clavier.nextFloat();
        System.out.println();
        
        System.out.println("Entrez votre poids :");
        poids = clavier.nextFloat();
        System.out.println();
        
        imc = 10000 * poids /(taille*taille);
        System.out.println("Votre imc est de : " + imc + "\n");
        
        if ( imc < 19){
            
            System.out.println("Vous etes maigre !");
            
        }
        
        else if (19 <= imc && imc <25){
            
            System.out.println("Vous etes normal !");
            
        }
        
        else if (25 <= imc && imc <30){
            
            System.out.println("Vous etes en surpoids !");
            
        }
        
        else{
            
            System.out.println("Vous etes obèse !");
            
        }        
        
    }
}
