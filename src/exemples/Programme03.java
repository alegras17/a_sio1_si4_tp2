package exemples;

import java.util.Scanner;

public class Programme03 {

    public static void main(String[] args) {
            
        float x, y;
        float total=0;
        float moyenne;
    
        Scanner  clavier=new Scanner(System.in);
        
        System.out.println("Entrez votre note d'oral");  // x note d'oral coeff 1
        x=clavier.nextFloat();
        
        System.out.println("Entrez votre note d'écrit"); // y note d'écrit coeff 2
        y=clavier.nextFloat();
      
        total= x+2*y;  // calcul du total
        
        moyenne=total/3;  // division par 3
        
        System.out.printf("Votre moyenne est: %4.1f\n", moyenne);
    
        
        if ( moyenne<10){
            System.out.println("Vous êtes recalé");
        }
        else { 
 
            if(moyenne<15){
             
                 System.out.println("Vous êtes reçu avec la mention ASSEZ BIEN");
            
            }else{
            
                 System.out.println("Vous êtes reçu avec la mention       BIEN");
                
            }
        }
        
    }
}
